# Generated from actioncable-5.0.0.rc2.gem by gem2rpm -*- rpm-spec -*-
%global gem_name actioncable

# Disabling JS recompilation might significantly reduce the amount of
# build dependencies.
%global recompile_js 1

Name: rubygem-%{gem_name}
Version: 5.0.0
Release: 1%{?dist}
Summary: WebSocket framework for Rails
Group: Development/Languages
License: MIT
URL: http://rubyonrails.org
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
# git clone https://github.com/rails/rails.git && cd rails/actioncable
# git checkout v5.0.0 && tar czvf actioncable-5.0.0-tests.tgz test/
Source1: %{gem_name}-%{version}-tests.tgz
# The source code of pregenerated JS files.
# git clone https://github.com/rails/rails.git && cd rails/actioncable
# git checkout v5.0.0 && tar czvf actioncable-5.0.0-app.tgz app/ Rakefile
Source2: %{gem_name}-%{version}-app.tgz
BuildRequires: ruby(release)
BuildRequires: rubygems-devel > 1.3.1
BuildRequires: ruby >= 2.2.2
BuildRequires: rubygem(actionpack) = %{version}
BuildRequires: rubygem(mocha)
BuildRequires: rubygem(nio4r)
BuildRequires: %{_bindir}/redis-server
BuildRequires: rubygem(redis)
BuildRequires: rubygem(websocket-driver)
%if 0%{?recompile_js} > 0
BuildRequires: rubygem(coffee-script)
BuildRequires: rubygem(rake)
BuildRequires: rubygem(sprockets)
BuildRequires: %{_bindir}/node
%endif
BuildArch: noarch

%description
Structure many real-time application concerns into channels over a single
WebSocket connection.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version} -a 2

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
%if 0%{?recompile_js} > 0
# Recompile the embedded JS file from CoffeeScript sources.
#
# This is practice suggested by packaging guidelines:
# https://fedoraproject.org/wiki/Packaging:Guidelines#Use_of_pregenerated_code

rm -rf lib/assets/compiled
RUBYOPT=-Ilib rake assets:compile
%endif

# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/


%check
pushd .%{gem_instdir}
tar xzvf %{SOURCE1}

# We don't have Puma in Fedora yet.
sed -i '/puma/ s/^/#/' test/test_helper.rb
mv test/client_test.rb{,.disable}

# We don't have em-hiredis in Fedora yet.
mv test/subscription_adapter/evented_redis_test.rb{,.disable}

# TODO: Needs AR together with PostgreSQL.
mv test/subscription_adapter/postgresql_test.rb{,.disable}

# Start a testing Redis server instance
REDIS_DIR=$(mktemp -d)
redis-server --dir $REDIS_DIR --pidfile $REDIS_DIR/redis.pid --daemonize yes

ruby -Ilib:test -e 'Dir.glob "./test/**/*_test.rb", &method(:require)'

# Shutdown Redis.
kill -INT $(cat $REDIS_DIR/redis.pid)

# TODO: Enable the test/javascript test cases.
popd

%files
%dir %{gem_instdir}
%license %{gem_instdir}/MIT-LICENSE
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/CHANGELOG.md
%doc %{gem_instdir}/README.md

%changelog
* Thu Jun 30 2016 Vít Ondruch <vondruch@redhat.com> - 5.0.0-1
- Initial package
